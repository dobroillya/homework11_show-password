const showPassword = document.querySelectorAll(".fas");
const inputs = document.querySelectorAll(".inputPassword");
const btn = document.querySelector(".btn");

showPassword.forEach((item) => {
  item.addEventListener("click", (e) => {
    e.target.classList.toggle("fa-eye");
    e.target.classList.toggle("fa-eye-slash");
    e.target.previousElementSibling.type === "password"
      ? (e.target.previousElementSibling.type = "text")
      : (e.target.previousElementSibling.type = "password");
  });
});

btn.addEventListener("click", (e) => {
  e.preventDefault();
  const value1 = document.querySelector(".inputPassword--first").value;
  const value2 = document.querySelector(".inputPassword--second").value;
  if (value1 !== value2) {
    alert(`Потрібно ввести однакові значення`);
  } else if (!value1) {
    alert(`Empty values!!!`);
  } else {
    alert(`You are welcome`);
  }
});
